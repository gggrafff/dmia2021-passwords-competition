# DMIA2021 Passwords competition

DMIA.ProductionML 2021.1 Password complexity  
https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords  

Regress words as best as possible.  
Target is a real value indicating how many times the password could be encountered in one million random passwords.  
